<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*

Route::group(['prefix' => 'databarang'], function(){
 
    Route::get('/', 'DataBarangController@index');
    Route::get('/create', 'DataBarangController@create');
    Route::post('/store', 'DataBarangController@store');
    Route::get('/show/{id}', 'DataBarangController@show');
    Route::post('/update/{id}', 'DataBarangController@update');
    Route::post('/destroy/{id}', 'DataBarangController@destroy');
 
});
*/