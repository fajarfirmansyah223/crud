<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpiredBarang extends Model
{
    protected $table = 'expired_barangs';
    protected $fillable = ['tanggal','waktu'];
    
}
