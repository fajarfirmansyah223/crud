<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $namabarang = Namabarangs::leftJoin('dosens', 'dosens.id', '=', 'ruangans.id_dosen')
            ->leftJoin('jurusans', 'jurusans.id', '=', 'ruangans.id_jurusan')
            ->leftJoin('mahasiswas', 'mahasiswas.id', '=', 'ruangans.id_mahasiswa')
            ->orderby('ruangans.created_at')
            ->select(
                    'ruangans.id',
                    'ruangans.nama_ruangan',
                    'dosens.id as id_dosen',
                    'dosens.nik',
                    'dosens.nama_dosen',
                    'dosens.jenis_kelamin as jenis_kelamin_dosen',
                    'jurusans.id as id_jurusan',
                    'jurusans.nama_jurusan',
                    'mahasiswas.id as id_mahasiswa',
                    'mahasiswas.nama',
                    'mahasiswas.tempat_tgl_lahir',
                    'mahasiswas.jenis_kelamin',
                    'mahasiswas.agama',
                    'mahasiswas.alamat'
                    )
            ->paginate(10);
           /* echo "<pre>";
            print_r($ruangans);
            echo "</pre>";
            exit();*/
        $start_page = (($ruangans->currentPage()-1) * 10) + 1;
        
        return view('mahasiswa.view',array('ruangans' =>$ruangans , 'start_page' => $start_page ));   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
