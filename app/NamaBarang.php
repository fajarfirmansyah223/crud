<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NamaBarang extends Model
{
    protected $table = 'nama_barangs';
    protected $fillable = ['kode_barang','nama_barang'];
}
