<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockBarang extends Model
{
     protected $table = 'stock_barangs';
     protected $fillable = ['jumlah_barang'];
}
