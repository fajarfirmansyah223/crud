<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeluarBarang extends Model
{
      protected $table    = 'keluar_barangs';
      protected $fillable = ['tanggal','waktu'];
}
