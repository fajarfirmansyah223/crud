@extends('layouts.app')
 
@section('content')
 
<div class="panel panel-info">
    <div class="panel-heading">
        <center>
        <h1>
        CRUD Laravel 5.3
        </h1>
        </center>
    </div>
    <div class="panel-body">
        <a href="{{ URL('mahasiswa/create') }}" class="btn btn-raised btn-primary pull-right">Tambah</a>
        {{-- part alert --}}
        
            {{-- Kita cek, jika sessionnya ada maka tampilkan alertnya, jika tidak ada maka tidak ditampilkan alertnya --}}
        
        @if (Session::has('after_update'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-dismissible alert-{{ Session::get('after_update.alert') }}">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ Session::get('after_update.title') }}</strong>
                      <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_update.text-1') }}</a> {{ Session::get('after_update.text-2') }}
                    </div>
                </div>
            </div>
            45t45t4
        @endif
        
        <table class="table table-bordered table-hover ">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Ruangan </th>
                    <th>Dosen </th>
                    <th>Jurusan </th>
                    <th>Nama </th>
                    <th>Tempat Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Agama</th>
                    <th>Alamat</th>
                    <th>Jurusan</th>
                    <th>Ruangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ruangans as $ruangan)
                    <tr>
                        <td>{{  $start_page }}</td>
                        <td>{{ $ruangan->nama_ruangan }}</td>
                        <td>{{ $ruangan->nama_dosen }}</td>
                        <td>{{ $ruangan->jurusan }}</td>
                        <td>{{ $ruangan->nama }}</td>
                        <td>{{ $ruangan->tempat_tgl_lahir }}</td>
                        <td>{{ $ruangan->jenis_kelamin }}</td>
                        <td>{{ $ruangan->agama }}</td>
                        <td>{{ $ruangan->alamat }}</td> 
                        <td>{{ $ruangan->jurusan }}</td>
                        <td>{{ $ruangan->ruangan }}</td>
                        <td>
                            <center>
                                <a href="{{ URL('ruangan/show') }}/{{ $ruangan->id }}" class="btn btn-sm btn-raised btn-info">Edit</a>
                                <a href="{{ URL('ruangan/destroy') }}/{{ $ruangan->id }}" class="btn btn-sm btn-raised btn-danger">Hapus</a>
                            </center>
                        </td>
                        --!>
                        <?php $start_page = $start_page +1 ?>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
 
@stop