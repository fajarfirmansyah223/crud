<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(NamaBarangTableSeeder::class);
        $this->call(StockBarangTableSeeder::class);
        $this->call(KeluarBarangTableSeeder::class); 
        $this->call(MasukBarangTableSeeder::class);
        $this->call(ExpiredBarangTableSeeder::class);
    }
}
