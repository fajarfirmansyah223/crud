<?php

use Illuminate\Database\Seeder;

class NamaBarangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('nama_barangs')->insert([
	        [
	            'kode_barang' => 'MT0001',
	            'nama_barang' => 'gerry salut'

	        ],
	        [

	            'kode_barang' => 'MT0002',
	            'nama_barang' => 'biskuit selmiut'


	        ],
	        [
	            'kode_barang' => 'MT0003',
	            'nama_barang' => 'susu dancow'
	        ]
	     ]);
    }
}
