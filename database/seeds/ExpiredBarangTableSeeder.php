<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ExpiredBarangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('expired_barangs')->insert([
            [
	            'tanggal' =>Carbon::now('Asia/Jakarta')->toDateString(),
	            'waktu'   => Carbon::now('Asia/Jakarta')->toTimeString(),
            ],
            [
                'tanggal' =>Carbon::now('Asia/Jakarta')->toDateString(),
                'waktu'   => Carbon::now('Asia/Jakarta')->toTimeString(),
            ],
            [

                'tanggal' =>Carbon::now('Asia/Jakarta')->toDateString(),
                'waktu'   => Carbon::now('Asia/Jakarta')->toTimeString(),
            ]
	            
	     ]);
	    
    }
}
